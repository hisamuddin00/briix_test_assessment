import 'package:flutter/material.dart';

class TextInputWidget extends StatelessWidget {
  const TextInputWidget({
    super.key,
    required this.label,
    this.hint,
    this.keyboardType = TextInputType.text,
    this.lines,
    this.validator,
    this.controller,
    this.maxLength,
  });

  final String label;
  final String? hint;

  final int? lines;
  final int? maxLength;

  final TextInputType keyboardType;

  final String? Function(String?)? validator;

  final TextEditingController? controller;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      maxLines: lines,
      minLines: lines,
      maxLength: maxLength,
      decoration: InputDecoration(
        labelText: label,
        hintText: hint ?? label,
        alignLabelWithHint: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      keyboardType: keyboardType,
      validator: validator,
    );
  }
}
