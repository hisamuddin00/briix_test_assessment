import 'package:auto_route/auto_route.dart';
import 'package:briix_assessment/core/utils/app_validator.dart';
import 'package:briix_assessment/fatures/movie/mobx/movie_form_mobx.dart';
import 'package:briix_assessment/fatures/movie/models/movie_model.dart';
import 'package:briix_assessment/fatures/movie/models/movie_genres.dart';
import 'package:briix_assessment/fatures/movie/widgets/text_input_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

@RoutePage()
class MovieFormPage extends StatelessWidget {
  const MovieFormPage({
    super.key,
    @PathParam('id') this.movieId,
    this.movieData,
  });

  final int? movieId;
  final MovieModel? movieData;

  @override
  Widget build(BuildContext context) {
    final movieFormMobx = GetIt.I.get<MovieFormMobx>();
    movieFormMobx.checkExisitingData(movieModel: movieData);

    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: movieId == null
            ? const Text('Insert New Movie')
            : const Text('Edit Movie Data'),
        actions: [
          IconButton(
            onPressed: movieId == null
                ? null
                : () => movieFormMobx.showDeleteConfirmation(
                      context: context,
                      id: movieId!,
                    ),
            icon: const Icon(Icons.delete),
          ),
          IconButton(
            onPressed: () {
              movieFormMobx.showSaveConfirmation(
                context: context,
                movieModel: movieData,
              );
            },
            icon: const Icon(Icons.save),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 10,
          ),
          child: Form(
            key: movieFormMobx.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextInputWidget(
                  controller: movieFormMobx.titleController,
                  label: 'Title',
                  hint: 'Enter movie title',
                  keyboardType: TextInputType.name,
                  validator: AppValidator.requiredField,
                ),
                const SizedBox(height: 20),
                TextInputWidget(
                  controller: movieFormMobx.directorController,
                  label: 'Director',
                  hint: 'Enter director name',
                  keyboardType: TextInputType.name,
                  validator: AppValidator.requiredField,
                ),
                const SizedBox(height: 20),
                TextInputWidget(
                  controller: movieFormMobx.summaryController,
                  label: 'Summary',
                  hint: 'Enter director name',
                  keyboardType: TextInputType.multiline,
                  lines: 3,
                  validator: AppValidator.requiredField,
                  maxLength: 100,
                ),
                const SizedBox(height: 20),
                Observer(
                  builder: (context) {
                    return Wrap(
                      spacing: 10,
                      children: [
                        for (var genre in MovieGenres.values)
                          FilterChip(
                            label: Text(genre.value),
                            selected: movieFormMobx.movieGenres.contains(genre),
                            onSelected: (bool selected) {
                              if (selected) {
                                movieFormMobx.addGenre(genre);
                              } else {
                                movieFormMobx.removeGenre(genre);
                              }
                            },
                          ),
                      ],
                    );
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
