class AppValidator {
  static String? requiredField(String? text) {
    if (text == null) {
      return 'This field is required';
    }
    if (text.trim().isEmpty) {
      return 'This field is required';
    }
    return null;
  }
}
