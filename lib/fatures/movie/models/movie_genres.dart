enum MovieGenres {
  action('Action'),
  animation('Animation'),
  drama('Drama'),
  scifi('Sci-Fi'),
  horror('Horror');

  const MovieGenres(this.value);
  final String value;
}
