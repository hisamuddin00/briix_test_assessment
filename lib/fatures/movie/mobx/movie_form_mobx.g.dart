// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_form_mobx.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieFormMobx on MovieFormMobxBase, Store {
  late final _$movieGenresAtom =
      Atom(name: 'MovieFormMobxBase.movieGenres', context: context);

  @override
  ObservableList<MovieGenres> get movieGenres {
    _$movieGenresAtom.reportRead();
    return super.movieGenres;
  }

  @override
  set movieGenres(ObservableList<MovieGenres> value) {
    _$movieGenresAtom.reportWrite(value, super.movieGenres, () {
      super.movieGenres = value;
    });
  }

  late final _$MovieFormMobxBaseActionController =
      ActionController(name: 'MovieFormMobxBase', context: context);

  @override
  void addGenre(MovieGenres genre) {
    final _$actionInfo = _$MovieFormMobxBaseActionController.startAction(
        name: 'MovieFormMobxBase.addGenre');
    try {
      return super.addGenre(genre);
    } finally {
      _$MovieFormMobxBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeGenre(MovieGenres genre) {
    final _$actionInfo = _$MovieFormMobxBaseActionController.startAction(
        name: 'MovieFormMobxBase.removeGenre');
    try {
      return super.removeGenre(genre);
    } finally {
      _$MovieFormMobxBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void showSaveConfirmation(
      {required BuildContext context, MovieModel? movieModel}) {
    final _$actionInfo = _$MovieFormMobxBaseActionController.startAction(
        name: 'MovieFormMobxBase.showSaveConfirmation');
    try {
      return super
          .showSaveConfirmation(context: context, movieModel: movieModel);
    } finally {
      _$MovieFormMobxBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void saveMovie({required BuildContext context, MovieModel? movieModel}) {
    final _$actionInfo = _$MovieFormMobxBaseActionController.startAction(
        name: 'MovieFormMobxBase.saveMovie');
    try {
      return super.saveMovie(context: context, movieModel: movieModel);
    } finally {
      _$MovieFormMobxBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void showDeleteConfirmation(
      {required BuildContext context, required int id}) {
    final _$actionInfo = _$MovieFormMobxBaseActionController.startAction(
        name: 'MovieFormMobxBase.showDeleteConfirmation');
    try {
      return super.showDeleteConfirmation(context: context, id: id);
    } finally {
      _$MovieFormMobxBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteMovie({required BuildContext context, required int id}) {
    final _$actionInfo = _$MovieFormMobxBaseActionController.startAction(
        name: 'MovieFormMobxBase.deleteMovie');
    try {
      return super.deleteMovie(context: context, id: id);
    } finally {
      _$MovieFormMobxBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void checkExisitingData({MovieModel? movieModel}) {
    final _$actionInfo = _$MovieFormMobxBaseActionController.startAction(
        name: 'MovieFormMobxBase.checkExisitingData');
    try {
      return super.checkExisitingData(movieModel: movieModel);
    } finally {
      _$MovieFormMobxBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
movieGenres: ${movieGenres}
    ''';
  }
}
