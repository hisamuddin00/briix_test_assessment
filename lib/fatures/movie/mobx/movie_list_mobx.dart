import 'package:briix_assessment/core/route/app_router.dart';
import 'package:briix_assessment/fatures/movie/data/movie_data.dart';
import 'package:briix_assessment/fatures/movie/models/movie_model.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';

part 'movie_list_mobx.g.dart';

class MovieListMobx = MovieListMobxBase with _$MovieListMobx;

abstract class MovieListMobxBase with Store {
  final movieData = MovieData();
  final appRouter = GetIt.I.get<AppRouter>();

  final searchTextController = TextEditingController();

  @observable
  var fullMovieList = ObservableList<MovieModel>();

  @action
  void getMovieList() {
    fullMovieList.clear();

    if (searchTextController.text.trim().isNotEmpty) {
      for (var movie in movieData.movieList) {
        if (movie.deleted == false &&
            movie.title
                .toLowerCase()
                .contains(searchTextController.text.toLowerCase())) {
          fullMovieList.add(movie);
        }
      }
    } else {
      for (var movie in movieData.movieList) {
        if (movie.deleted == false) {
          fullMovieList.add(movie);
        }
      }
    }
  }
}
