import 'package:flutter/material.dart';

class EmptyMovieListWidget extends StatelessWidget {
  const EmptyMovieListWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('No movie to be viewed'),
    );
  }
}
