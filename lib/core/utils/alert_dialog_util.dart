import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

class AlertDialogUtil {
  static void showConfirmationDialog({
    required BuildContext context,
    required String title,
    required String message,
    required void Function() onYesPressed,
  }) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: [
            TextButton(
              child: const Text("No"),
              onPressed: () {
                context.router.pop();
              },
            ),
            TextButton(
              onPressed: () {
                context.router.pop(true);
                onYesPressed();
              },
              child: const Text("Yes"),
            ),
          ],
        );
      },
    ).then(
      (value) {
        if (value != null) context.router.pop();
      },
    );
  }
}
