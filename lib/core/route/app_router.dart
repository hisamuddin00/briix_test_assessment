import 'package:auto_route/auto_route.dart';
import 'package:briix_assessment/core/route/app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          page: MovieListRoute.page,
          path: '/',
          initial: true,
        ),
        AutoRoute(
          page: MovieFormRoute.page,
          path: '/movie-form/new',
        ),
        AutoRoute(
          page: MovieFormRoute.page,
          path: '/movie-form/:id',
        ),
      ];
}
