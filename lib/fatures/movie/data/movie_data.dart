import 'package:briix_assessment/fatures/movie/models/movie_genres.dart';
import 'package:briix_assessment/fatures/movie/models/movie_model.dart';

class MovieData {
  var movieList = <MovieModel>[];

  void saveMovie({
    required String title,
    required String director,
    required String summary,
    required List<MovieGenres> genres,
  }) {
    var id = movieList.length + 1;

    var movieModel = MovieModel(
      id: id,
      title: title,
      director: director,
      summary: summary,
      genres: genres,
    );

    movieList.add(movieModel);
  }

  void editMovie({
    required int id,
    required String title,
    required String director,
    required String summary,
    required List<MovieGenres> genres,
  }) {
    var index = movieList.indexWhere((element) => element.id == id);

    movieList[index].title = title;
    movieList[index].director = director;
    movieList[index].summary = summary;
    movieList[index].genres = genres;
  }

  void deleteMovie({
    required int movieId,
  }) {
    var index = movieList.indexWhere((element) => element.id == movieId);

    movieList[index].deleted = true;
  }
}
