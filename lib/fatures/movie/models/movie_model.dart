import 'package:briix_assessment/fatures/movie/models/movie_genres.dart';

class MovieModel {
  int id;
  String title;
  String director;
  String summary;
  List<MovieGenres> genres;
  bool deleted;

  MovieModel({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genres,
    this.deleted = false,
  });
}
