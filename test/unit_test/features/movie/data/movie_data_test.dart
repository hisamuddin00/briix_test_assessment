import 'package:briix_assessment/fatures/movie/data/movie_data.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Movie list test', () {
    final movieData = MovieData();
    test('Movie list start with empty data', () {
      expect(movieData.movieList.length, 0);
    });

    test(
        'Movie list id should 1 when insert first movie and data should sama with parameter',
        () {
      movieData.saveMovie(
        title: 'title test',
        director: 'director test',
        summary: 'summary test',
        genres: [],
      );

      expect(movieData.movieList.length, 1);
      expect(movieData.movieList.first.id, 1);
      expect(movieData.movieList.first.title, 'title test');
      expect(movieData.movieList.first.director, 'director test');
      expect(movieData.movieList.first.summary, 'summary test');
      expect(movieData.movieList.first.genres, []);
    });

    test('Movie id should increment to 2 when insert second data', () {
      movieData.saveMovie(
        title: 'title test',
        director: 'director test',
        summary: 'summary test',
        genres: [],
      );

      expect(movieData.movieList.last.id, 2);
    });

    test('Movie data edit should change data with parameter', () {
      movieData.editMovie(
        id: 2,
        title: 'title test 2',
        director: 'director test 2',
        summary: 'summary test 2',
        genres: [],
      );

      expect(movieData.movieList.last.title, 'title test 2');
      expect(movieData.movieList.last.director, 'director test 2');
      expect(movieData.movieList.last.summary, 'summary test 2');
      expect(movieData.movieList.last.genres, []);
    });

    test(
        'Delete movie should not remove movie from list, but mark is as deleted',
        () {
      movieData.deleteMovie(
        movieId: 2,
      );

      expect(movieData.movieList.last.deleted, true);
    });
  });
}
