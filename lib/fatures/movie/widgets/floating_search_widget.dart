import 'package:flutter/material.dart';

class FloatingSearchWidget extends StatelessWidget
    implements PreferredSizeWidget {
  const FloatingSearchWidget({
    super.key,
    required this.controller,
    required this.onChanged,
  });

  final TextEditingController controller;
  final Function() onChanged;

  @override
  Size get preferredSize => const Size.fromHeight(70);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      padding: const EdgeInsets.only(
        left: 20,
        top: 5,
        bottom: 5,
      ),
      margin: EdgeInsets.only(
        left: 20,
        right: 20,
        top: MediaQuery.paddingOf(context).top,
      ),
      child: TextFormField(
        controller: controller,
        decoration: const InputDecoration(
          suffixIcon: Icon(Icons.search),
          border: InputBorder.none,
          hintText: 'Type to seacrh movie by title',
        ),
        onChanged: (value) => onChanged(),
      ),
    );
  }
}
