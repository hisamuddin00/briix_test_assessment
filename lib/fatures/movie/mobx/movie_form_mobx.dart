import 'package:auto_route/auto_route.dart';
import 'package:briix_assessment/fatures/movie/mobx/movie_list_mobx.dart';
import 'package:briix_assessment/fatures/movie/models/movie_model.dart';
import 'package:briix_assessment/fatures/movie/models/movie_genres.dart';
import 'package:briix_assessment/core/utils/alert_dialog_util.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';

part 'movie_form_mobx.g.dart';

class MovieFormMobx = MovieFormMobxBase with _$MovieFormMobx;

abstract class MovieFormMobxBase with Store {
  final movieListMobx = GetIt.I.get<MovieListMobx>();

  final formKey = GlobalKey<FormState>();

  final titleController = TextEditingController();
  final directorController = TextEditingController();
  final summaryController = TextEditingController();

  @observable
  var movieGenres = ObservableList<MovieGenres>();

  @action
  void addGenre(MovieGenres genre) {
    if (!movieGenres.contains(genre)) {
      movieGenres.add(genre);
    }
  }

  @action
  void removeGenre(MovieGenres genre) {
    if (movieGenres.contains(genre)) {
      movieGenres.removeWhere((e) => e == genre);
    }
  }

  @action
  void showSaveConfirmation({
    required BuildContext context,
    MovieModel? movieModel,
  }) {
    if (!formKey.currentState!.validate()) {
      return;
    }

    AlertDialogUtil.showConfirmationDialog(
      context: context,
      title: 'Save Movie',
      message: 'Are you sure want to save this movie?',
      onYesPressed: () => saveMovie(
        context: context,
        movieModel: movieModel,
      ),
    );
  }

  @action
  void saveMovie({
    required BuildContext context,
    MovieModel? movieModel,
  }) {
    if (!formKey.currentState!.validate()) {
      return;
    }

    if (movieModel == null) {
      movieListMobx.movieData.saveMovie(
        title: titleController.text,
        director: directorController.text,
        summary: summaryController.text,
        genres: movieGenres,
      );
    } else {
      movieListMobx.movieData.editMovie(
        id: movieModel.id,
        title: titleController.text,
        director: directorController.text,
        summary: summaryController.text,
        genres: movieGenres,
      );
    }

    movieListMobx.getMovieList();
  }

  @action
  void showDeleteConfirmation({
    required BuildContext context,
    required int id,
  }) {
    AlertDialogUtil.showConfirmationDialog(
      context: context,
      title: 'Delete Movie',
      message: 'Are you sure want to delete this movie?',
      onYesPressed: () => deleteMovie(
        context: context,
        id: id,
      ),
    );
  }

  @action
  void deleteMovie({
    required BuildContext context,
    required int id,
  }) {
    movieListMobx.movieData.deleteMovie(movieId: id);

    context.router.pop();

    movieListMobx.getMovieList();
  }

  @action
  void checkExisitingData({MovieModel? movieModel}) {
    if (movieModel != null) {
      titleController.text = movieModel.title;
      directorController.text = movieModel.director;
      summaryController.text = movieModel.summary;

      movieGenres.clear();
      for (var genre in movieModel.genres) {
        movieGenres.add(genre);
      }
    }
  }
}
