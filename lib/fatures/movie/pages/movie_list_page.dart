import 'package:auto_route/auto_route.dart';
import 'package:briix_assessment/core/route/app_router.gr.dart';
import 'package:briix_assessment/fatures/movie/mobx/movie_list_mobx.dart';
import 'package:briix_assessment/fatures/movie/widgets/empty_movie_list_widget.dart';
import 'package:briix_assessment/fatures/movie/widgets/floating_search_widget.dart';
import 'package:briix_assessment/fatures/movie/widgets/movie_card_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

@RoutePage()
class MovieListPage extends StatelessWidget {
  const MovieListPage({super.key});

  @override
  Widget build(BuildContext context) {
    final movieListMobx = GetIt.I.get<MovieListMobx>();

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: FloatingSearchWidget(
        controller: movieListMobx.searchTextController,
        onChanged: movieListMobx.getMovieList,
      ),
      body: Observer(
        builder: (_) {
          if (movieListMobx.fullMovieList.isEmpty) {
            return const EmptyMovieListWidget();
          }
          return ListView.builder(
            itemBuilder: (context, index) {
              return MovieCardWidget(
                movieModel: movieListMobx.fullMovieList[index],
                onTap: () {
                  context.router.push(
                    MovieFormRoute(
                      movieId: movieListMobx.fullMovieList[index].id,
                      movieData: movieListMobx.fullMovieList[index],
                    ),
                  );
                },
              );
            },
            itemCount: movieListMobx.fullMovieList.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.router.push(
            MovieFormRoute(),
          );
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
