// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i3;
import 'package:briix_assessment/fatures/movie/models/movie_model.dart' as _i5;
import 'package:briix_assessment/fatures/movie/pages/movie_form_page.dart'
    as _i1;
import 'package:briix_assessment/fatures/movie/pages/movie_list_page.dart'
    as _i2;
import 'package:flutter/material.dart' as _i4;

abstract class $AppRouter extends _i3.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    MovieFormRoute.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<MovieFormRouteArgs>(
          orElse: () => MovieFormRouteArgs(movieId: pathParams.optInt('id')));
      return _i3.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i1.MovieFormPage(
          key: args.key,
          movieId: args.movieId,
          movieData: args.movieData,
        ),
      );
    },
    MovieListRoute.name: (routeData) {
      return _i3.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.MovieListPage(),
      );
    },
  };
}

/// generated route for
/// [_i1.MovieFormPage]
class MovieFormRoute extends _i3.PageRouteInfo<MovieFormRouteArgs> {
  MovieFormRoute({
    _i4.Key? key,
    int? movieId,
    _i5.MovieModel? movieData,
    List<_i3.PageRouteInfo>? children,
  }) : super(
          MovieFormRoute.name,
          args: MovieFormRouteArgs(
            key: key,
            movieId: movieId,
            movieData: movieData,
          ),
          rawPathParams: {'id': movieId},
          initialChildren: children,
        );

  static const String name = 'MovieFormRoute';

  static const _i3.PageInfo<MovieFormRouteArgs> page =
      _i3.PageInfo<MovieFormRouteArgs>(name);
}

class MovieFormRouteArgs {
  const MovieFormRouteArgs({
    this.key,
    this.movieId,
    this.movieData,
  });

  final _i4.Key? key;

  final int? movieId;

  final _i5.MovieModel? movieData;

  @override
  String toString() {
    return 'MovieFormRouteArgs{key: $key, movieId: $movieId, movieData: $movieData}';
  }
}

/// generated route for
/// [_i2.MovieListPage]
class MovieListRoute extends _i3.PageRouteInfo<void> {
  const MovieListRoute({List<_i3.PageRouteInfo>? children})
      : super(
          MovieListRoute.name,
          initialChildren: children,
        );

  static const String name = 'MovieListRoute';

  static const _i3.PageInfo<void> page = _i3.PageInfo<void>(name);
}
